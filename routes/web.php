<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' =>['auth']], function(){
Route::get('/', 'HomeController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/bannerImage', 'AdBannerController');
    
Route::resource('/notice', 'NoticeController');
Route::get('/ajaxGetNoticeFile', 'NoticeController@getNoticeFile')->name('ajaxGetNoticeFile');
Route::delete('/noticefile/{id}/delete', 'NoticeController@deleteNoticeFile')->name('deleteNoticeFile');

Route::resource('/contact', 'ContactController');

Route::resource('/note', 'NoteController');
Route::get('/ajaxGetNoteFile', 'NoteController@getNoteFile')->name('ajaxGetNoteFile');
Route::delete('/notefile/{id}/delete', 'NoteController@deleteNoteFile')->name('deleteNoteFile');

Route::resource('/result', 'ResultController');
Route::get('/ajaxGetResultFile', 'ResultController@getResultFile')->name('ajaxGetResultFile');
Route::delete('/resultfile/{id}/delete', 'ResultController@deleteResultFile')->name('deleteResultFile');

Route::resource('/article', 'ArticleController');
Route::get('/ajaxGetArticleFile', 'ArticleController@getArticleFile')->name('ajaxGetArticleFile');
Route::delete('/articlefile/{id}/delete', 'ArticleController@deleteArticleFile')->name('deleteArticleFile');

Route::resource('/syllabi', 'SyllabusController');
Route::get('/ajaxGetSyllabusFile', 'SyllabusController@getSyllabusFile')->name('ajaxGetSyllabusFile');
Route::delete('/syllabusfile/{id}/delete', 'SyllabusController@deleteSyllabusFile')->name('deleteSyllabusFile');

Route::resource('/question', 'OldQuestionController');
Route::get('/ajaxGetOldQuestionFile', 'OldQuestionController@getOldQuestionFile')->name('ajaxGetOldQuestionFile');
Route::delete('/oldquestionfile/{id}/delete', 'OldQuestionController@deleteOldQuestionFile')->name('deleteOldQuestionFile');

Route::resource('/taxonomies', 'TaxonomyController');
Route::get('/ajaxGetTaxonomyFile', 'TaxonomyController@getTaxonomyFile')->name('ajaxGetTaxonomyFile');
Route::delete('/taxonomyfile/{id}/delete', 'TaxonomyController@deleteTaxonomyFile')->name('deleteTaxonomyFile');

Route::resource('/capsule', 'CapsuleController');
Route::get('/ajaxGetCapsuleFile', 'CapsuleController@getCapsuleFile')->name('ajaxGetCapsuleFile');
Route::delete('/capsulefile/{id}/delete', 'CapsuleController@deleteCapsuleFile')->name('deleteCapsuleFile');


Route::resource('/department', 'DepartmentController');
Route::resource('/category', 'CategoryController');

Route::get('/booking', 'BookingController@index')->name('booking.index');
Route::get('/booking/{id}', 'BookingController@view')->name('booking.view');
Route::get('/booking/delete/{id}', 'BookingController@delete')->name('booking.destroy');
});