<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

	// Route::get('/contact', 'Api\v1\ContactController@getContact');	
Route::group(['middleware'=>['api'],'prefix'=>'/v1'], function(){
	Route::post('/contact', 'Api\v1\ContactController@postContact');	
	Route::get('/note/filter', 'Api\v1\NoteController@filter');
	Route::get('/oldquestion/filter', 'Api\v1\OldQuestionController@filter');
	Route::get('/result/filter', 'Api\v1\ResultController@filter');
	Route::get('/article/filter', 'Api\v1\ArticleController@filter');
	Route::get('/taxonomy/filter', 'Api\v1\TaxonomyController@filter');
	Route::get('/syllabus', 'Api\v1\SyllabusController@filter');
	Route::get('/notice', 'Api\v1\NoticeController@getNotice');	
	Route::get('/category', 'Api\v1\CapsuleController@getCategory');	
	Route::get('/capsule', 'Api\v1\CapsuleController@getCapsule');	
	Route::get('/department', 'Api\v1\NoteController@getDepartment');
	Route::get('/bannerImage', 'Api\v1\ArticleController@bannerImage');
	Route::post('/booking', 'Api\v1\BookingController@bookingRequest');
});
