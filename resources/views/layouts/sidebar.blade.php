<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        @auth
        <p>{{Auth::user()->name}}</p>
        @endauth
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">s
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{route('booking.index')}}">
          <i class="fa fa-th"></i> <span>Booking</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='department') class="active" @endif>
        <a href="{{route('department.index')}}">
          <i class="fa fa-building"></i> <span>Department</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='bannerImage') class="active" @endif>
        <a href="{{route('bannerImage.index')}}">
          <i class="fa fa-image"></i> <span>Ad Banner</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='notice') class="active" @endif>
        <a href="{{route('notice.index')}}">
          <i class="fa fa-file-pdf-o"></i> <span>Notices</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='note') class="active" @endif>
        <a href="{{route('note.index')}}">
          <i class="fa fa-files-o"></i> <span>Notes</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='article') class="active" @endif>
        <a href="{{route('article.index')}}">
          <i class="fa fa-newspaper-o"></i> <span>Article</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='result') class="active" @endif>
        <a href="{{route('result.index')}}">
          <i class="fa fa-sticky-note-o"></i> <span>Result</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='syllabi') class="active" @endif>
        <a href="{{route('syllabi.index')}}">
          <i class="fa fa-file-o"></i> <span>Syllabus</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='contact') class="active" @endif>
        <a href="{{route('contact.index')}}">
          <i class="fa fa-envelope-o"></i> <span>Message</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='question') class="active" @endif>
        <a href="{{route('question.index')}}">
          <i class="fa fa-question-circle-o"></i> <span>OldQuestion</span>
        </a>
      </li>
      <li @if(Request::segment(1)=='taxonomies') class="active" @endif>
        <a href="{{route('taxonomies.index')}}">
          <i class="fa fa-file-pdf-o"></i> <span>Taxonomy</span>
        </a>
      </li>
      <li class="treeview"  @if(Request::segment(1)=='category') class='active' @endif>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Entrance Preparation Capsule</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li @if(Request::segment(1)=='category') class="active" @endif><a href="{{route('category.index')}}"><i class="fa fa-circle-o"></i>Category</a></li>
          <li @if(Request::segment(1)=='capsule') class="active" @endif><a href="{{route('capsule.index')}}"><i class="fa fa-circle-o"></i> Capsule Entry</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>