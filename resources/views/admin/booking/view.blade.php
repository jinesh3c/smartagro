@extends('layouts.app')
@section('styles')
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-10">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Booking View</h3>
              </div>
              <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                      <td>Name</td>
                      <td>{{$booking->name}}</td>
                    </tr>   
                    <tr>
                      <td>Email</td>
                      <td>{{$booking->email}}</td>
                    </tr> 
                    <tr>
                      <td>Phone</td>
                      <td>{{$booking->phone}}</td>
                    </tr>  
                    <tr>
                      <td>Address</td>
                      <td>{{$booking->address}}</td>
                    </tr>  
                    <tr>
                      <td>College</td>
                      <td>{{$booking->college}}</td>
                    </tr>  
                    <tr>
                      <td>Payment From</td>
                      <td>{{$booking->payment_from}}</td>
                    </tr>  
                    <tr>
                      <td>Shift</td>
                      <td>{{$booking->shift}}</td>
                    </tr>  
                    <tr>
                      <td>Amount</td>
                      <td>{{$booking->amount}}</td>
                    </tr>   
                    <tr>
                      <td>Voucher</td>
                      <td><a href="{{$booking->image}}" target="_blank"><img src="{{$booking->image}}" width="100px;"></a></td>
                    </tr>                 
                </table>
            </div>
        </div>
    </div>
  </div>
</div>
@endSection
@section('scripts')
@endsection