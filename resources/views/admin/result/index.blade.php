@extends('layouts.app')
@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<script src="{{asset('adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-10">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Result</h3>
                <div class="pull-right">
                <a href="{{route('result.create')}}" class="btn btn-xs btn-primary">Add Result</a>
                </div>
              </div>
              <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                   <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>Year</th>
                    <th>File</th>
                    <th>Action</th>
                  </tr>
                 </thead>
                 <tbody>
                 @foreach($results as $k=>$result)
                 <tr>
                   <td>{{$k+1}}</td>
                   <td>{{$result->title}}</td>
                   <td>{{$result->year}}</td>
                   <td>
                     @foreach($result->result_file as $file)
                      <li>
                        <a href="{{asset($file->file_name)}}">{{$file->file_title}}</a>
                      </li>
                     @endforeach
                   </td>
                   <td>
                     <form action="{{route('result.destroy',$result->id)}}" method="post">
                     <a href="{{route('result.edit',$result->id)}}" class="btn btn-xs btn-warning">EDIT</a>
                      {{csrf_field()}}
                      {{method_field('DELETE')}}
                      <input type="submit" class="btn btn-xs btn-danger" value="DELETE">
                     </form>
                   </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
               <div class="text-center">
                 {{ $results->links() }}
               </div>  
            </div>
        </div>
    </div>
  </div>
</div>
@endSection
@section('scripts')
<script src="{{asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
//data table
    $('#dataTable').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false
    });
</script>
@endsection