@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Add Category</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('category.update',$category->id)}}" enctype="multipart/form-data">
                   {{csrf_field()}}
                   {{method_field('PUT')}}
                   <div class="form-group">
                      <label>Category Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input name="category" type="text" class="form-control @error('category') is-invalid @enderror" value="{{ $category->title }}" placeholder="category title" required>
                      </div>
                      @if ($errors->has('category'))
                          <span class="help-block">
                              <strong>{{ $errors->first('category') }}</strong>
                          </span>
                      @endif
                   </div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endSection
@section('scripts')
<script>
  
</script>
@endsection