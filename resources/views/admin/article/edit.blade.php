@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Edit Article</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('article.update',$article->id)}}" enctype="multipart/form-data">
                   {{csrf_field()}}
                   {{method_field('PUT')}}
                   <div class="form-group">
                      <label>Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="title" id="title" placeholder="title" value="{{$article->title}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Select Category:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="tag" class="form-control">
                            <option value="{{null}}">Select Category</option>
                            <option value="{{'news'}}" @if($article->tag=='news') selected @endif>News</option>
                            <option value="{{'literature'}}" @if($article->tag=='literature') selected @endif>Literature</option>
                            <option value="{{'blog'}}" @if($article->tag=='blog') selected @endif>Blog</option>
                          </select>
                      </div>
                  </div>
                   <div class="form-group">
                      <label>Description:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div> 
                          <textarea name="description" id="editor" class="form-control">{{$article->description}}</textarea>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>File Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="file_title" placeholder="file title">
                      </div>
                   </div>
                   <div class="form-group">
                      <label>File:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]">
                      </div>
                   </div>
                   <div id="file-action" class="row"></div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endSection
@section('scripts')
<script src="{{asset('adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<script>
CKEDITOR.replace("editor");
  $(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = $('#title').val();
    if(data){
      $.ajax({
          url: '{{route('ajaxGetArticleFile')}}',
          data:{
              _token: CSRF_TOKEN,
              id: <?php echo $article->id; ?>,
          },
          dataType: 'JSON',
          success:function(data){
              console.log(data.msg)
              var files = data.msg
              $('#file-action').html('');
              $.each(files, function(index, value){
                var url = "{{url('/articlefile')}}"+"/"+(value.id)+'/delete';
                $('#file-action').append('<div class="col-md-2">'+(value.file_title)+'<form method="post" action='+url+'>{{csrf_field()}}{{method_field('DELETE')}}<button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></form></div>');
                  // $('#file-action').append($('<option>',{value:value.id}).text(value.name));
                  console.log(value);
              });
          }
      })
    }
  })
</script>
@endsection