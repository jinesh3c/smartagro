@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Edit OldQuestion</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('question.update',$question->id)}}" enctype="multipart/form-data">
                   {{csrf_field()}}
                   {{method_field('PUT')}}
                   <div class="form-group">
                      <label>Department:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="department_id" class="form-control" required>
                            <option value="{{null}}">Select Department</option>
                            @foreach($departments as $department)
                            <option value="{{$department->id}}" @if($question->department_id==$department->id) selected @endif>{{$department->title}}</option>
                            @endforeach
                          </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Semester:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="semester" class="form-control" required>
                            <option value="{{null}}">Select Semester</option>
                            <option value="{{'1'}}" @if($question->semester_id=='1') selected @endif>semester one</option>
                            <option value="{{'2'}}" @if($question->semester_id=='2') selected @endif>semester two</option>
                            <option value="{{'3'}}" @if($question->semester_id=='3') selected @endif>semester three</option>
                            <option value="{{'4'}}" @if($question->semester_id=='4') selected @endif>semester four</option>
                            <option value="{{'5'}}"@if($question->semester_id=='5') selected @endif>semester five</option>
                            <option value="{{'6'}}"@if($question->semester_id=='6') selected @endif>semester six</option>
                            <option value="{{'7'}}"@if($question->semester_id=='7') selected @endif>semester seven</option>
                            <option value="{{'8'}}"@if($question->semester_id=='8') selected @endif>semester eight</option>
                          </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Subject:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="subject" id="subject" placeholder="subject" value="{{$question->subject}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>File Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="file_title" placeholder="file title">
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Old Question File:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]">
                      </div>
                   </div>
                   <div id="file-action" class="row"></div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endSection
@section('scripts')
<script>

  $(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = $('#subject').val();
    if(data){
      $.ajax({
          url: '{{route('ajaxGetOldQuestionFile')}}',
          data:{
              _token: CSRF_TOKEN,
              id: <?php echo $question->id; ?>,
          },
          dataType: 'JSON',
          success:function(data){
              console.log(data.msg)
              var files = data.msg
              $('#file-action').html('');
              $.each(files, function(index, value){
                var url = "{{url('/oldquestionfile')}}"+"/"+(value.id)+'/delete';
                  $('#file-action').append('<div class="col-md-2">'+(value.file_title)+'<form method="post" action='+url+'>{{csrf_field()}}{{method_field('DELETE')}}<button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></form></div>');
                  // $('#file-action').append($('<option>',{value:value.id}).text(value.name));
                  console.log(value);
              });
          }
      })
    }
  })
</script>
@endsection