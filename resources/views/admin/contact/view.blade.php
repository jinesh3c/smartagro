@extends('layouts.app')
@section('styles')
@endsection
@section('content')
<script src="{{asset('adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-6">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Message</h3>
              </div>
              <div class="box-body">
                  <div class="col-md-6">
                    <p>From:</p>
                    <p>Email:</p>
                    <p>subject:</p>
                    <p>message:</p>
                  </div>
                  <div class="col-md-6">
                    <p>{{$contact->name}}</p>
                    <p>{{$contact->email}}</p>
                    <p>{{$contact->subject}}</p>
                    <p>{{$contact->message}}</p>
                  </div>
            </div>
        </div>
    </div>
  </div>
</div>
@endSection
@section('scripts')

@endsection