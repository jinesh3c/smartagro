@extends('layouts.app')
@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<script src="{{asset('adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-10">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Message</h3>
              </div>
              <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                   <tr>
                    <th>SN</th>
                    <th>name</th>
                    <th>email</th>
                    <th>subject</th>
                    <th>message</th>
                    <th>Action</th>
                  </tr>
                 </thead>
                 <tbody>
                 @foreach($contacts as $k=>$contact)
                 <tr>
                   <td>{{$k+1}}</td>
                   <td>{{$contact->name}}</td>
                   <td>{{$contact->email}}</td>
                   <td>{{$contact->subject}}</td>
                   <td>{{$contact->message}}</td>
                   <td>
                     <a href="{{route('contact.show',$contact->id)}}" class="btn btn-xs btn-info">View</a>
                   </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
               <div class="text-center">
                 {{ $contacts->links() }}
               </div>  
            </div>
        </div>
    </div>
  </div>
</div>
@endSection
@section('scripts')
<script src="{{asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
//data table
    $('#dataTable').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false
    });
</script>
@endsection