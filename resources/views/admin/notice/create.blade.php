@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Add Notice</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('notice.store')}}" enctype="multipart/form-data">
                   {{csrf_field()}}
                   <div class="form-group">
                      <label>Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="title" placeholder="title" required>
                      </div>
                   </div>
                   <div class="form-group">
                    <label>Description:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-building"></i>
                        </div>
                        <textarea name="description" class="form-control"></textarea>
                    </div>
                 </div>
                   <div class="form-group">
                      <label>File Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="file_title" placeholder="file title" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Notice File:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]">
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Notice Image:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="images[]">
                      </div>
                   </div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endSection
@section('scripts')
<script>
  
</script>
@endsection