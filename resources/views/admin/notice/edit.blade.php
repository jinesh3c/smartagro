@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Edit Notice</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('notice.update',$notice->id)}}" enctype="multipart/form-data">
                   {{csrf_field()}}
                   {{method_field('PUT')}}
                   <div class="form-group">
                      <label>Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="title" id="subject" placeholder="title" value="{{$notice->title}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Description:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <textarea name="description" class="form-control">{{$notice->description}}</textarea> 
                      </div>
                  </div>
                   <div class="form-group">
                      <label>File Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="file_title" placeholder="file title">
                      </div>
                  </div>
                   <div class="form-group">
                      <label>Notice FIle:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]">
                      </div>
                   </div>
                   
                   <div class="form-group">
                      <label>Images:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="images[]">
                      </div>
                   </div>
                   <div id="file-action" class="row"></div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endSection
@section('scripts')
<script>

  $(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = $('#subject').val();
    if(data){
      $.ajax({
          url: '{{route('ajaxGetNoticeFile')}}',
          data:{
              _token: CSRF_TOKEN,
              id: <?php echo $notice->id; ?>,
          },
          dataType: 'JSON',
          success:function(data){
              console.log(data.msg)
              var files = data.msg
              $('#file-action').html('');
              $.each(files, function(index, value){
                  var url = "{{url('/noticefile')}}"+"/"+(value.id)+'/delete';
                  $('#file-action').append('<div class="col-md-2">'+(value.file_title)+'<form method="post" action='+url+'>{{csrf_field()}}{{method_field('DELETE')}}<button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></form></div>');
                  // $('#file-action').append($('<option>',{value:value.id}).text(value.name));
                  console.log(value);
              });
          }
      })
    }
    
  })
</script>
@endsection