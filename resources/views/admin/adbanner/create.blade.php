@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Add Ad Banner Image</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('bannerImage.store')}}" enctype="multipart/form-data">
                   {{csrf_field()}}
                   <div class="form-group">
                      <label>File Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="file_title" placeholder="file title" required>
                      </div>
                   </div>
                   <div class="form-group">
                    <label>Status:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-building"></i>
                        </div>
                        <select name="status" class="form-control">
                          <option value="enable">Enable</option>
                          <option value="disable">Disable</option>
                        </select>
                    </div>
                 </div>
                   <div class="form-group">
                      <label>Image:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="image" required>
                      </div>
                   </div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endSection
