@extends('layouts.app')
@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<script src="{{asset('adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-10">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">AdBanner</h3>
                <div class="pull-right">
                <a href="{{route('bannerImage.create')}}" class="btn btn-xs btn-primary">Add AdBanner</a>
                </div>
              </div>
              <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                   <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>File</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                 </thead>
                 <tbody>
                 @foreach($banners as $k=>$banner)
                 <tr>
                   <td>{{$k+1}}</td>
                   <td>{{$banner->file_title}}</td>
                   <td>
                     <img src="{{url($banner->file_name)}}" alt="{{$banner->file_title}}" width="100px">
                    </td>
                    <td>{{$banner->status}}</td>
                   <td>
                     <form action="{{route('bannerImage.destroy',$banner->id)}}" method="post">
                     <a href="{{route('bannerImage.edit',$banner->id)}}" class="btn btn-xs btn-warning">EDIT</a>
                      {{csrf_field()}}
                      {{method_field('DELETE')}}
                      <input type="submit" class="btn btn-xs btn-danger" value="DELETE">
                     </form>
                   </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
               <div class="text-center">
                 {{ $banners->links() }}
               </div>  
            </div>
        </div>
    </div>
  </div>
</div>
@endSection
@section('scripts')
<script src="{{asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
//data table
    $('#dataTable').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false
    });
</script>
@endsection