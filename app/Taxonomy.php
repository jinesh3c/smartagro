<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxonomy extends Model
{
    public function taxonomy_file(){
    	return $this->hasMany('App\TaxonomyFile');
    }
}
