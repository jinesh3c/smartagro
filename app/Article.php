<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function article_file(){
    	return $this->hasMany('App\ArticleFile');
    }
}
