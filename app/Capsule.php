<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capsule extends Model
{
    public function category(){
    	return $this->belongsTo('App\Category');
    }
    public function capsule_file(){
    	return $this->hasMany('App\CapsuleFile');
    }
}
