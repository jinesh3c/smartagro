<?php

namespace App\Http\Controllers;

use File;
use App\Taxonomy;
use App\TaxonomyFile;
use Illuminate\Http\Request;

class TaxonomyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxonomies = Taxonomy::orderBy('id','desc')->paginate(10);
        return view('admin.taxonomy.index',compact('taxonomies'));
    }
    public function getTaxonomyFile(Request $request){
        $data = TaxonomyFile::where('taxonomy_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.taxonomy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'file_title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = new Taxonomy;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/taxonomy/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/taxonomy/', $name);  
                $dataFile = new TaxonomyFile;
                $dataFile->taxonomy_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('taxonomies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $taxonomy = Taxonomy::find($id);
        return view('admin.taxonomy.edit',compact('taxonomy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'file_title' => 'sometimes',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = Taxonomy::find($id);
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/taxonomy/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/taxonomy/', $name);  
                $dataFile = new TaxonomyFile;
                $dataFile->taxonomy_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('taxonomies.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = TaxonomyFile::where('taxonomy_id',$id)->get();
        foreach($files as $file){
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $file->delete();
        }
        Taxonomy::find($id)->delete();
        return redirect()->route('taxonomies.index');
    }
    public function deleteTaxonomyFile($id)
    {
        $file = TaxonomyFile::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
