<?php

namespace App\Http\Controllers;

use File;   
use App\AdBanner;
use Illuminate\Http\Request;

class AdBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = AdBanner::orderBy('created_at','desc')->paginate(10);
        return view('admin.adbanner.index',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.adbanner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file_title' => 'sometimes',
            'status' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
        ]);
        if($request->hasfile('image'))
        {
            $image =$request->file('image');
            $title=$image->getClientOriginalName();
            $name = '/banner/'.str_random(10).'.'.$image->getClientOriginalExtension();
            $image->move(public_path().'/banner/', $name);
            $dataFile = new AdBanner;
            $dataFile->file_title = $request->file_title;
            $dataFile->status = $request->status;
            $dataFile->file_name = $name;
            $dataFile->save();
        }
        return redirect()->route('bannerImage.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = AdBanner::find($id);
        return view('admin.adbanner.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'file_title' => 'sometimes',
            'status' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $dataFile = AdBanner::find($id);
        $dataFile->file_title = $request->file_title;
        $dataFile->status = $request->status;
        if($request->hasfile('image'))
        {
            $file = AdBanner::find($id);
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $image =$request->file('image');
            $title=$image->getClientOriginalName();
            $name = '/banner/'.str_random(10).'.'.$image->getClientOriginalExtension();
            $image->move(public_path().'/banner/', $name);
            $dataFile->file_name = $name;
        }
        $dataFile->save();
        return redirect()->route('bannerImage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = AdBanner::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->route('bannerImage.index');
    }
}
