<?php

namespace App\Http\Controllers;

use File;
use App\Article;
use App\ArticleFile;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('id','desc')->paginate(10);
        return view('admin.article.index',compact('articles'));
    }
    public function getArticleFile(Request $request){
        $data = ArticleFile::where('article_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'tag' => 'required',
            'file_title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);
        $data = new Article;
        $data->title = $request->title;
        $data->tag = $request->tag;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/articles/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/articles/', $name);  
                $dataFile = new ArticleFile;
                $dataFile->article_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->image_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        return view('admin.article.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'tag' => 'required',
            'file_title' => 'sometimes',
            'files' => 'sometimes',
            'files.*' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);
        $data = Article::find($id);
        $data->title = $request->title;
        $data->tag = $request->tag;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/articles/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/articles/', $name);  
                $dataFile = new ArticleFile;
                $dataFile->article_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->image_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('article.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = ArticleFile::where('article_id',$id)->get();
        foreach($files as $file){
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $file->delete();
        }
        Article::find($id)->delete();
        return redirect()->route('article.index');
    }
    public function deleteArticleFile($id)
    {
        $file = ArticleFile::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
