<?php

namespace App\Http\Controllers;

use File;
use App\Capsule;
use App\Category;
use App\CapsuleFile;
use Illuminate\Http\Request;

class CapsuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $capsules = Capsule::orderBy('id','desc')->paginate(10);
        return view('admin.capsule.index',compact('capsules'));
    }
    public function getCapsuleFile(Request $request){
        $data = CapsuleFile::where('capsule_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.capsule.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'category' => 'required',
            'file_title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = new Capsule;
        $data->category_id = $request->category;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/capsules/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/capsules/', $name);  
                $dataFile = new CapsuleFile;
                $dataFile->capsule_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('capsule.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $capsule = Capsule::find($id);
        return view('admin.capsule.edit',compact('capsule','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'category' => 'required',
            'file_title' => 'sometimes',
            'files' => 'sometimes',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = Capsule::find($id);
        $data->title = $request->title;
        $data->category_id = $request->category;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/capsules/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/capsules/', $name);  
                $dataFile = new CapsuleFile;
                $dataFile->capsule_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('capsule.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = CapsuleFile::where('capsule_id',$id)->get();
        foreach($files as $file){
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $file->delete();
        }
        Capsule::find($id)->delete();
        return redirect()->route('capsule.index');
    }
    public function deleteCapsuleFile($id)
    {
        $file = CapsuleFile::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
