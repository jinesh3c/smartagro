<?php

namespace App\Http\Controllers;

use File;
use App\OldQuestion;
use App\Department;
use App\OldQuestionFile;
use Illuminate\Http\Request;

class OldQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = OldQuestion::orderBy('id','desc')->paginate(10);
        return view('admin.oldquestion.index',compact('questions'));
    }
    public function getOldQuestionFile(Request $request){
        $data = OldQuestionFile::where('oldQuestion_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('admin.oldquestion.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'department_id' => 'required|integer',
            'semester' => 'required',
            'subject' => 'required',
            'file_title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = new OldQuestion;
        $data->department_id = $request->department_id;
        $data->semester_id = $request->semester;
        $data->subject = $request->subject;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/oldquestion/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/oldquestion/', $name);  
                $dataFile = new OldQuestionFile;
                $dataFile->oldQuestion_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('question.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = OldQuestion::find($id);
        $departments = Department::all();
        return view('admin.oldquestion.edit',compact('question','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'department_id' => 'required|integer',
            'semester' => 'required',
            'file_title' => 'sometimes',
            'subject' => 'required',
            'files' => 'sometimes',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = OldQuestion::find($id);
        $data->department_id = $request->department_id;
        $data->semester_id = $request->semester;
        $data->subject = $request->subject;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/oldquestion/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/oldquestion/', $name);  
                $dataFile = new OldQuestionFile;
                $dataFile->oldQuestion_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('question.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = OldQuestionFile::where('oldQuestion_id',$id)->get();
        foreach($files as $file){
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $file->delete();
        }
        OldQuestion::find($id)->delete();
        return redirect()->route('question.index');
    }
    public function deleteOldQuestionFile($id)
    {
        $file = OldQuestionFile::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
