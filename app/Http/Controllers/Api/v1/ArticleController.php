<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use App\Article;
use App\AdBanner;
use Validator;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class ArticleController extends BaseController
{
    use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /** 
        *   @OA\get(
        *     path="/article/filter",
        *     tags={"Search"},
        *     description="article filter",
        *     summary="article filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "year":"2019",
        *                   "offset":"1",
        *                   "limit":"10",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function filter(Request $request)
    {
    	$data = Article::orderBy('created_at','desc')
    				->with('article_file')
    				->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No article found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'article list success');
    }
    /** 
        *   @OA\get(
        *     path="/bannerImage",
        *     tags={"Search"},
        *     description="bannerImage filter",
        *     summary="bannerImage filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="bannerImage filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function bannerImage(Request $request)
    {
        $data = AdBanner::orderBy('created_at','desc')->where('status','enable')->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No banner found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'banner list success');
    }
}
