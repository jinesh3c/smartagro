<?php

namespace App\Http\Controllers\Api\v1;

use App\Notice;
use App\NoticeFile;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class NoticeController extends BaseController
{
    /** 
        *   @OA\get(
        *     path="/notice",
        *     tags={"Search"},
        *     description="Notice",
        *     summary="Notice",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "department":"5",
        *                   "semester":"semester one",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function getNotice(Request $request)
    {
    	$data = Notice::orderBy('created_at','desc')->with('notice_file')->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No notice matched your query');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'notice list success');
    }
}
