<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use App\Syllabus;
use Validator;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class SyllabusController extends BaseController
{
	use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /** 
        *   @OA\get(
        *     path="/syllabus",
        *     tags={"Search"},
        *     description="syllabus filter",
        *     summary="syllabus filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "offset":"1",
        *                   "limit":"10",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function filter(Request $request)
    {
    	$data = Syllabus::orderBy('created_at','desc')
    				->with('syllabus_file')
    				->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No syllabus found!');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'note list success');
    }
}
