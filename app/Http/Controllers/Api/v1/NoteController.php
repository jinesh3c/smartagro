<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use App\Note;
use Validator;
use App\Department;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class NoteController extends BaseController
{
	use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
	/** 
        *   @OA\get(
        *     path="/note/filter",
        *     tags={"Search"},
        *     description="Note filter",
        *     summary="Note filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Parameter(name="department", in="query", description="department",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Parameter(name="semester", in="query", description="semester",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "department":"5",
        *                   "semester":"semester one",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function filter(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'department'  => 'required|integer',
            'semester'  => 'required|integer',
        ]);

        if($validator->fails()){
            $this->setStatusCode(Res::HTTP_UNPROCESSABLE_ENTITY);
            return $this->respondValidationError('Validation Error.', $validator->errors());
        }
    	$data = Note::where('department_id',$request->department)
    				->where('semester_id',$request->semester)
    				->with('note_file')
    				->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No note matched your query');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'note list success');
    }
    /** 
        *   @OA\get(
        *     path="/department",
        *     tags={"Search"},
        *     description="department filter",
        *     summary="department filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="department filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function getDepartment(Request $request)
    {
        $data = Department::all();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No department found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'department list success');
    }
}
