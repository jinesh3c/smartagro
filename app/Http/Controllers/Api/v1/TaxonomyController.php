<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Validator;
use App\Taxonomy;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class TaxonomyController extends BaseController
{
    /** 
        *   @OA\get(
        *     path="/taxonomy/filter",
        *     tags={"Search"},
        *     description="taxonomy filter",
        *     summary="taxonomy filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "year":"2019",
        *                   "offset":"1",
        *                   "limit":"10",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function filter(Request $request)
    {
    	$data = Taxonomy::orderBy('created_at','desc')
    				->with('taxonomy_file')
    				->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No taxonomy found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'taxonomy list success');
    }
}
