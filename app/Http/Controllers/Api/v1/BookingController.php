<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Validator;
use App\Booking;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class BookingController extends BaseController
{
    use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /**
        * @OA\post(
        *     path="/booking",
        *     tags={"booking"},
        *     description="send message",
        *     summary="send message to admin admins",
        *     security= {{"App_Key":"",}},
        *     @OA\RequestBody(
        *         description="conatct send message",
        *         required=true,
        *          @OA\MediaType(
        *             mediaType="multipart/form-data",
        *             @OA\Schema(
        *                 type="object",
        *                 @OA\Property(property="name",description="name",type="string",),
        *                 @OA\Property(property="email",description="email",type="string",),
        *                 @OA\Property(property="address",description="address",type="string",),
        *                 @OA\Property(property="phone",description="phone",type="string",),
        *                 @OA\Property(property="course",description="course",type="string",),
        *                 @OA\Property(property="college",description="college",type="string",),
        *                 @OA\Property(property="amount",description="amount",type="string",),
        *                 @OA\Property(property="shift",enum={"morning","day"}),
        *                 @OA\Property(property="payment_from",enum={"bank","e-sewa"}),
        *                 @OA\Property(property="image",description="image",type="file"),
        *             ),
        *           ),
        *     ),
        *     @OA\Response(
        *         response=200,
        *         description="message sent successfully",
        *         @OA\JsonContent(
        *             type="object",
        *         ),
        *         @OA\Link(
        *              link="index",
        *              operationId="message",
        *           ),
        *     ),
        *     @OA\Response(
        *          response="default",
        *          description="an ""unexpected"" error",
        *          @OA\JsonContent(
        *             type="object",
        *          ),
        *      ),
        * )
    */
    public function bookingRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|string',
            'email'  => 'required',
            'address'  => 'required',
            'phone'  => 'required',
            'payment_from'  => 'required|in:bank,e-sewa,ime,khalti,others',
            'college'  => 'sometimes',
            'shift'  => 'required|in:morning,day',
            'amount'  => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
        ]);
        if($validator->fails()){
            $this->setStatusCode(Res::HTTP_UNPROCESSABLE_ENTITY);
            return $this->respondValidationError('Validation Error.', $validator->errors());
        }
        $data = new Booking;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->address = $request->address;
        $data->college = $request->college;
        $data->phone = $request->phone;
        $data->payment_from = $request->payment_from;
        $data->shift = $request->shift;
        $data->course = $request->course;
        $data->amount = $request->amount;
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $name = '/booking/'.str_random(10).'.'.$file->getClientOriginalExtension();
            $file->move(public_path().'/booking/',$name);
            $data->image = $name;
        }
        $data->save();
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'booking success');
    }
}
