<?php

namespace App\Http\Controllers\Api\v1;

use Validator;
use App\OldQuestion;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class OldQuestionController extends BaseController
{
    use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /** 
        *   @OA\get(
        *     path="/oldquestion/filter",
        *     tags={"Search"},
        *     description="old question filter",
        *     summary="old question filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Parameter(name="department", in="query", description="department",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Parameter(name="semester", in="query", description="semester",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "department":"5",
        *                   "semester":"1",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function filter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'department'  => 'required|integer',
            'semester'  => 'required|integer',
        ]);

        if($validator->fails()){
            $this->setStatusCode(Res::HTTP_UNPROCESSABLE_ENTITY);
            return $this->respondValidationError('Validation Error.', $validator->errors());
        }
        $data = OldQuestion::where('department_id',$request->department)
                    ->where('semester_id',$request->semester)
                    ->with('oldQuestion_file')
                    ->orderBy('created_at','desc')
                    ->get();
        if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No question found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'question list success');
    }
}
