<?php

namespace App\Http\Controllers\Api\v1;

use App\Category;
use App\Capsule;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class CapsuleController extends BaseController
{
    use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /** 
        *   @OA\get(
        *     path="/category",
        *     tags={"Search"},
        *     description="getCategory",
        *     summary="getCategory",
        *     security= {{"App_Key":"",}},
        *     @OA\Response(response=200,description="getCategory successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="getCategory",
        *             operationId="getCategory",
        *             parameters={
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function getCategory(Request $request)
    {
    	$data = Category::all();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No category found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'category list success');
    }
    /** 
        *   @OA\get(
        *     path="/capsule",
        *     tags={"Search"},
        *     description="getCategory",
        *     summary="getCategory",
        *     security= {{"App_Key":"",}},
        *     @OA\Parameter(name="category_id", in="query", description="category_id",
        *          @OA\Schema(type="string",),
        *      ),
        *     @OA\Response(response=200,description="getCategory successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="getCategory",
        *             operationId="getCategory",
        *             parameters={
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function getCapsule(Request $request)
    {
    	$data = Capsule::where('category_id',$request->category_id)->with('capsule_file')->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No Capsule found');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'Capsule list success');
    }
}
