<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;

class BookingController extends Controller
{
    public function index(Request $request)
    {
        $bookings = Booking::orderBy('created_at','desc')->paginate(20);
        return view('admin.booking.index', compact('bookings'));
    }
    public function view($id)
    {
        $booking = Booking::find($id);
        return view('admin.booking.view', compact('booking'));
    }
    public function delete($id)
    {
        Booking::find($id)->delete();
        return redirect()->route('booking.index');
    }
}
