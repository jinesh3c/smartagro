<?php

namespace App\Http\Controllers;

use File;
use App\Result;
use App\ResultFile;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::orderBy('id','desc')->paginate(10);
        return view('admin.result.index',compact('results'));
    }
    public function getResultFile(Request $request){
        $data = ResultFile::where('result_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.result.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'file_title' => 'required',
            'year' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = new Result;
        $data->title = $request->title;
        $data->year = $request->year;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/results/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/results/', $name);  
                $dataFile = new ResultFile;
                $dataFile->result_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('result.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Result::find($id);
        return view('admin.result.edit',compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'file_title' => 'sometimes',
            'year' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = Result::find($id);
        $data->title = $request->title;
        $data->year = $request->year;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/results/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/results/', $name);  
                $dataFile = new ResultFile;
                $dataFile->result_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('result.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = ResultFile::where('result_id',$id)->get();
        foreach($files as $file){
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $file->delete();
        }
        Result::find($id)->delete();
        return redirect()->route('result.index');
    }
    public function deleteResultFile($id)
    {
        $file = ResultFile::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
