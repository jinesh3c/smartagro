<?php

namespace App\Http\Controllers;

use File;
use App\Note;
use App\Department;
use App\NoteFile;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::orderBy('id','desc')->paginate(10);
        return view('admin.note.index',compact('notes'));
    }
    public function getNoteFile(Request $request){
        $data = NoteFIle::where('note_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('admin.note.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'department_id' => 'required|integer',
            'semester' => 'required',
            'subject' => 'required',
            'file_title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = new Note;
        $data->department_id = $request->department_id;
        $data->semester_id = $request->semester;
        $data->subject = $request->subject;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/notes/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/notes/', $name);  
                $dataFile = new NoteFile;
                $dataFile->note_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('note.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Note::find($id);
        $departments = Department::all();
        return view('admin.note.edit',compact('note','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'department_id' => 'required|integer',
            'semester' => 'required',
            'subject' => 'required',
            'file_title' => 'sometimes',
            'files' => 'sometimes',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = Note::find($id);
        $data->department_id = $request->department_id;
        $data->semester_id = $request->semester;
        $data->subject = $request->subject;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/notes/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/notes/', $name);  
                $dataFile = new NoteFile;
                $dataFile->note_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('note.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = NoteFile::where('note_id',$id)->get();
        foreach($files as $file){
            if (File::exists(public_path($file->file_name))) {
                File::delete(public_path($file->file_name));
            }
            $file->delete();
        }
        Note::find($id)->delete();
        return redirect()->route('note.index');
    }
    public function deleteNoteFile($id)
    {
        $file = NoteFile::find($id);
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
