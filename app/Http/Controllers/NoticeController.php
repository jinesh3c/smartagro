<?php

namespace App\Http\Controllers;

use App\Notice;
use App\NoticeFile;
use File;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::orderBy('created_at','desc')->paginate(10);
        return view('admin.notice.index',compact('notices'));
    }
    public function getNoticeFile(Request $request){
        $data = NoticeFile::where('notice_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'file_title' => 'required',
            'files' => 'sometimes',
            'files.*' => 'mimes:doc,pdf,docx',
            'images' => 'sometimes',
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);
        $data = new Notice;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/notices/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/notices/', $name);
                $dataFile = new NoticeFile;
                $dataFile->notice_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
         if($request->hasfile('images'))
         {
            foreach($request->file('images') as $image)
            {
                $title=$image->getClientOriginalName();
                $name = '/notices/'.str_random(10).'.'.$image->getClientOriginalExtension();
                $image->move(public_path().'/notices/', $name);
                $dataFile = new NoticeFile;
                $dataFile->notice_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->image_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('notice.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::find($id);
        return view('admin.notice.edit',compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'file_title' => 'sometimes',
            'files' => 'sometimes',
            'files.*' => 'mimes:doc,pdf,docx',
            'images' => 'sometimes',
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);
        $data = Notice::find($id);
        $data->title = $request->title;
        $data->description = $request->description;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/notices/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/notices/', $name);
                $dataFile = new NoticeFile;
                $dataFile->notice_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
         if($request->hasfile('images'))
         {
            foreach($request->file('images') as $image)
            {
                $title=$image->getClientOriginalName();
                $name = '/notices/'.str_random(10).'.'.$image->getClientOriginalExtension();
                $image->move(public_path().'/notices/', $name);
                $dataFile = new NoticeFile;
                $dataFile->notice_id = $data->id;
                $dataFile->file_title = $request->file_title;
                $dataFile->image_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('notice.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = NoticeFile::where('notice_id',$id)->get();
        foreach($files as $file){
            if($file->file_name){
                if (File::exists(public_path($file->file_name))) {
                    File::delete(public_path($file->file_name));
                }
            }else{
                if (File::exists(public_path($file->image_name))) {
                    File::delete(public_path($file->image_name));
                }
            }
            $file->delete();
        }
        Notice::find($id)->delete();
        return redirect()->route('notice.index');
    }
    public function deleteNoticeFile($id)
    {
        $file = NoticeFile::find($id);
        if (File::exists(public_path($file->image_name))) {
            File::delete(public_path($file->image_name));
        }
        if (File::exists(public_path($file->file_name))) {
            File::delete(public_path($file->file_name));
        }
        $file->delete();
        return redirect()->back();
    }
}
