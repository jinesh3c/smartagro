<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    public function notice_file(){
    	return $this->hasMany('App\NoticeFile','notice_id');
    }
}
