<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldQuestion extends Model
{
    public function department(){
    	return $this->belongsTo('App\Department');
    }
    public function semester(){
    	return $this->belongsTo('App\Semester');
    }
    public function oldQuestion_file(){
    	return $this->hasMany('App\OldQuestionFile','oldQuestion_id');
    }
}
