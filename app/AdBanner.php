<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdBanner extends Model
{
    protected $table = 'ad_banners';    
}
