<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    public function result_file(){
    	return $this->hasMany('App\ResultFile');
    }
}
