<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Semester;
use App\Department;
use Illuminate\Support\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::Create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        Department::create([
        	'title' => 'agriculture'
        ]);
        Department::create([
        	'title' => 'forestry'
        ]);
        Department::create([
        	'title' => 'vetenary'
        ]);
        Semester::create([
            'title' => 'semester one'
        ]);
        Semester::create([
            'title' => 'semester two'
        ]);
        Semester::create([
            'title' => 'semester three'
        ]);
        Semester::create([
            'title' => 'semester four'
        ]);
        Semester::create([
            'title' => 'semester five'
        ]);
        Semester::create([
            'title' => 'semester six'
        ]);
        Semester::create([
            'title' => 'semester seven'
        ]);
        Semester::create([
            'title' => 'semester eight'
        ]);
    }
}
